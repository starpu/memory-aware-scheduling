#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 1 1920 12 5 2000 no_prio 4 Cholesky_dependances
#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 4 1920 12 5 2000 no_prio 4 Cholesky_dependances
#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 8 1920 15 5 2000 no_prio 4 Cholesky_dependances

#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 1 1920 12 5 2000 best_ones 4 Cholesky_dependances
#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 2 1920 12 5 2000 best_ones 6 Cholesky_dependances
#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 4 1920 12 5 2000 best_ones 4 Cholesky_dependances
#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 8 1920 15 5 2000 best_ones 4 Cholesky_dependances

#~ # bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 1 1920 12 5 32000 best_ones 4 Cholesky_dependances
#~ # bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 2 1920 12 5 32000 best_ones 4 Cholesky_dependances
#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 4 1920 12 5 32000 best_ones 4 Cholesky_dependances
#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 8 1920 15 5 32000 best_ones 4 Cholesky_dependances


# bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 1 1920 7 6 2000 best_ones 4 LU
# bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 2 1920 7 6 2000 best_ones 4 LU
#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 4 1920 7 6 2000 best_ones 4 LU
# bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 8 1920 10 6 2000 best_ones 4 LU

#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 1 1920 10 6 32000 best_ones 4 LU
# bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 2 1920 7 6 32000 best_ones 4 LU
# bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 4 1920 7 6 32000 best_ones 4 LU
# bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 8 1920 7 6 32000 best_ones 4 LU



#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 16 320 10 10 2000 best_ones 4 Out_of_core_lu
#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 16 320 10 8 500 best_ones 4 Out_of_core_lu
#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 48 320 10 8 500 best_ones 4 Out_of_core_lu
#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 48 320 10 8 2000 best_ones 4 Out_of_core_lu
#~ bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 48 320 10 8 1000 best_ones 4 Out_of_core_lu


# Courbes dans article ICPP:
bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 1 1920 12 5 2000 best_ones 4 Cholesky_dependances
bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 8 1920 15 5 2000 best_ones 4 Cholesky_dependances
bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 8 1920 15 5 32000 best_ones 4 Cholesky_dependances
bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 4 1920 7 6 2000 best_ones 4 LU
bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 1 1920 10 6 32000 best_ones 4 LU
bash Scripts_maxime/PlaFRIM-Grid5k/draw_cholesky_dependances.sh 48 320 10 8 2000 best_ones 4 Out_of_core_lu
