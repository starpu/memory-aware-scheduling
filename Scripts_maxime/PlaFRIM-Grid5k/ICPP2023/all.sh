bash Scripts_maxime/PlaFRIM-Grid5k/cholesky_dependances.sh 1 1920 12 2000 best_ones
bash Scripts_maxime/PlaFRIM-Grid5k/cholesky_dependances.sh 8 1920 15 2000 best_ones
bash Scripts_maxime/PlaFRIM-Grid5k/cholesky_dependances.sh 8 1920 15 0 best_ones

bash Scripts_maxime/PlaFRIM-Grid5k/lu.sh 4 1920 7 2000 best_ones
bash Scripts_maxime/PlaFRIM-Grid5k/lu.sh 1 1920 10 0 best_ones

bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 2000 $(($1)) modular-eager-prefetching
bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 2000 $(($1)) dmdas
bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 2000 $(($1)) lws
bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 2000 $(($1)) dynamic-data-aware
