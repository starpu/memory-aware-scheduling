#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 500 16 lws
#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 500 16 dynamic-data-aware
#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 500 16 dmdas
#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 500 16 modular-eager-prefetching

#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 500 48 lws
#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 500 48 dynamic-data-aware
#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 500 48 dmdas
#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 500 48 modular-eager-prefetching

#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 2000 48 lws
#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 2000 48 dynamic-data-aware
#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 2000 48 dmdas
#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 2000 48 modular-eager-prefetching

#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 1000 48 lws
#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 1000 48 dynamic-data-aware
#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 1000 48 dmdas
#bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 1000 48 modular-eager-prefetching


bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 2000 $(($1)) modular-eager-prefetching
bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 2000 $(($1)) dmdas
bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 2000 $(($1)) lws
bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 10 8 2000 $(($1)) dynamic-data-aware
