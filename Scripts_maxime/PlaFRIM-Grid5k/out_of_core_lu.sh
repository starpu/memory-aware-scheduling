# bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 12 5 1000 2 modular-eager-prefetching
# bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 12 5 1000 2 dmdas
# bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 12 5 1000 2 dmdar
# bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 12 5 1000 2 lws
# bash Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 12 5 1000 2 dynamic-data-aware

# Exemple de commande:
# oarsub -p nova -l walltime=01:00:00 'bash -l -c "module load openmpi/4.1.3_gcc-10.2.0;" && Scripts_maxime/PlaFRIM-Grid5k/out_of_core_lu.sh 320 8 10 1000 10 dynamic-data-aware'

#module load openmpi/4.1.3_gcc-10.2.0
#make clean
#bash script_initialisation_starpu_maxime_sans_simgrid_grid5k

# PENSER A CALIBRATE AVEC DMDAR!!

if [ $# != 6 ]
then
	echo "Arguments must be: TAILLE_TUILE NB_TAILLE_TESTE ECHELLE_X MEMOIRE NCPU SCHEDULER"
	exit
fi

START_X=0
ECHELLE_X=$3

TAILLE_TUILE=$1
NB_TAILLE_TESTE=$2
CM=$4
NCPU=$5
SCHEDULER=$6
TH=10
CP=5
SEED=1
NCOMBINAISONS=7

echo ${CM}"Mo -" ${NCPU} "cpus -" ${TAILLE_TUILE} "tiles -" ${ECHELLE_X} "echelle -" ${NB_TAILLE_TESTE} "tailles testées -" ${SCHEDULER}
	
FICHIER_RAW=Output_maxime/Data/Out_of_core_lu/GF_${TAILLE_TUILE}_${CM}Mo_${NCPU}CPUs_${SCHEDULER}.txt
FICHIER_RAW_DT=Output_maxime/Data/Out_of_core_lu/DT_${TAILLE_TUILE}_${CM}Mo_${NCPU}CPUs_${SCHEDULER}.txt
FICHIER_BUS=Output_maxime/Data/Out_of_core_lu/BUS_${TAILLE_TUILE}_${CM}Mo_${NCPU}CPUs_${SCHEDULER}.txt

truncate -s 0 ${FICHIER_RAW}
truncate -s 0 ${FICHIER_RAW_DT}
truncate -s 0 ${FICHIER_BUS}

for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
do
	N=$((START_X+i*ECHELLE_X))
	echo "N=${N}"
	if [ ${SCHEDULER} == "dynamic-data-aware" ]; then
		echo "CPU_ONLY=1"
		STARPU_NCPU=$((NCPU)) CPU_ONLY=1 STARPU_BUS_STATS=1 STARPU_BUS_STATS_FILE="${FICHIER_BUS}" THRESHOLD=0 DOPT_SELECTION_ORDER=7 CHOOSE_BEST_DATA_FROM=0 SIMULATE_MEMORY=0 CAN_A_DATA_BE_IN_MEM_AND_IN_NOT_USED_YET=0 PRIORITY_ATTRIBUTION=1 HIGHEST_PRIORITY_TASK_RETURNED_IN_DEFAULT_CASE=1 GRAPH_DESCENDANTS=0 PUSH_FREE_TASK_ON_GPU_WITH_LEAST_TASK_IN_PLANNED_TASK=2 STARPU_SCHED_READY=1 TASK_ORDER=2 DATA_ORDER=2 FREE_PUSHED_TASK_POSITION=1 DEPENDANCES=1 PRIO=1 APP=1 EVICTION_STRATEGY_DYNAMIC_DATA_AWARE=1 STARPU_DISK_SWAP_BACKEND=unistd_o_direct STARPU_LIMIT_CPU_MEM=$((CM)) STARPU_NCUDA=0 SEED=$((N/5)) STARPU_SCHED=${SCHEDULER} STARPU_NTASKS_THRESHOLD=$((TH)) STARPU_CUDA_PIPELINE=$((CP)) STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_NOPENCL=0 ./mpi/examples/mpi_lu/plu_outofcore_example_double -size $((TAILLE_TUILE*N)) -nblocks $((N)) -path /tmp | tail -n 1 >> ${FICHIER_RAW}
		sed -n '6,'$((NCOMBINAISONS))'p' ${FICHIER_BUS} >> ${FICHIER_RAW_DT}
	else
		STARPU_NCPU=$((NCPU)) STARPU_BUS_STATS=1 STARPU_BUS_STATS_FILE="${FICHIER_BUS}" STARPU_DISK_SWAP_BACKEND=unistd_o_direct STARPU_LIMIT_CPU_MEM=$((CM)) STARPU_NCUDA=0 SEED=$((N/5)) STARPU_SCHED=${SCHEDULER} STARPU_NTASKS_THRESHOLD=$((TH)) STARPU_CUDA_PIPELINE=$((CP)) STARPU_EXPECTED_TRANSFER_TIME_WRITEBACK=1 STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_NOPENCL=0 ./mpi/examples/mpi_lu/plu_outofcore_example_double -size $((TAILLE_TUILE*N)) -nblocks $((N)) -path /tmp | tail -n 1 >> ${FICHIER_RAW}
		sed -n '6,'$((NCOMBINAISONS))'p' ${FICHIER_BUS} >> ${FICHIER_RAW_DT}
	fi
done

