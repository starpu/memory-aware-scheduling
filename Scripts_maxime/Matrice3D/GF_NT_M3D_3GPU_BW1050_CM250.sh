#!/usr/bin/bash
#~ bash Scripts_maxime/Matrice3D/GF_NT_M3D_3GPU_BW1050_CM250.sh /home/gonthier /home/gonthier/these_gonthier_maxime/Starpu 2 ZN 8 63 2800
#~ bash Scripts_maxime/Matrice3D/GF_NT_M3D_3GPU_BW1050_CM250.sh /home/gonthier /home/gonthier/these_gonthier_maxime/Starpu 2 ZN 3 250 1050

#~ bash Scripts_maxime/Matrice3D/GF_NT_M3D_3GPU_BW1050_CM250.sh /home/gonthier /home/gonthier/these_gonthier_maxime/Starpu 2 Z4 8 63 2800
#~ ou Z4
PATH_STARPU=$1
PATH_R=$2
NB_TAILLE_TESTE=$3 
Z=$4
NGPU=$5
CUDAMEM=$6
BW=$7
export STARPU_PERF_MODEL_DIR=tools/perfmodels/sampling
ulimit -S -s 5000000
NB_ALGO_TESTE=8
FICHIER=GF_NT_M3D_MULTIGPU_${Z}_${NGPU}_${CUDAMEM}_${BW}
FICHIER_RAW=${PATH_STARPU}/starpu/Output_maxime/GFlops_raw_out_3.txt
DOSSIER=Matrice3D
truncate -s 0 ${FICHIER_RAW:0}

if [ $Z == "Z4" ]
then
ECHELLE_X=5
START_X=0  
	echo "############## Modular eager prefetching ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=modular-eager-prefetching STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_CUDA_PIPELINE=30 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xy $((960*N)) -nblocks $((N)) -nblocksz 4 -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
	echo "############## Dmdar ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=dmdar STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_CUDA_PIPELINE=30 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xy $((960*N)) -nblocks $((N)) -nblocksz 4 -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
	echo "############## HEFT ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=modular-heft STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_CUDA_PIPELINE=30 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xy $((960*N)) -nblocks $((N)) -nblocksz 4 -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
	echo "############## MST ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=mst STARPU_NTASKS_THRESHOLD=30 STARPU_CUDA_PIPELINE=30 STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xy $((960*N)) -nblocks $((N)) -nblocksz 4 -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
	echo "############## CM ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=cuthillmckee REVERSE=1 STARPU_NTASKS_THRESHOLD=30 STARPU_CUDA_PIPELINE=30 STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xy $((960*N)) -nblocks $((N)) -nblocksz 4 -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
	#~ echo "############## HFP U TH30 MULTIGPU0 + TASK STEALING ##############"
	#~ for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		#~ do 
		#~ N=$((START_X+i*ECHELLE_X))
		#~ STARPU_SCHED=HFP STARPU_SCHED_READY=1 STARPU_NTASKS_THRESHOLD=30 TASK_STEALING=3 STARPU_SCHED_READY=1 STARPU_CUDA_PIPELINE=30 MULTIGPU=1 ORDER_U=1 STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xy $((960*N)) -nblocks $((N)) -nblocksz 4 -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	#~ done
	echo "############## HFP U TH30 MULTIGPU4 + TASK STEALING ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=HFP STARPU_SCHED_READY=1 STARPU_NTASKS_THRESHOLD=30 TASK_STEALING=3 STARPU_SCHED_READY=1 STARPU_CUDA_PIPELINE=30 MULTIGPU=4 ORDER_U=1 STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xy $((960*N)) -nblocks $((N)) -nblocksz 4 -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
	echo "############## Modular heft idle HFP U MULTIGPU4 TH0 ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=modular-heft-HFP STARPU_SCHED_READY=1 MODULAR_HEFT_HFP_MODE=2 MULTIGPU=4 STARPU_NTASKS_THRESHOLD=0 STARPU_CUDA_PIPELINE=30 ORDER_U=1 STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xy $((960*N)) -nblocks $((N)) -nblocksz 4 -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
fi
if [ $Z == "ZN" ]
then
ECHELLE_X=3
START_X=0
	echo "############## Modular eager prefetching ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=modular-eager-prefetching STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_CUDA_PIPELINE=30 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xyz $((960*N)) -nblocks $((N)) -nblocksz $((N)) -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
	echo "############## Dmdar ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=dmdar STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_CUDA_PIPELINE=30 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xyz $((960*N)) -nblocks $((N)) -nblocksz $((N)) -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
	echo "############## HEFT ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=modular-heft STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_CUDA_PIPELINE=30 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xyz $((960*N)) -nblocks $((N)) -nblocksz $((N)) -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
	echo "############## MST ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=mst STARPU_NTASKS_THRESHOLD=30 STARPU_CUDA_PIPELINE=30 STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xyz $((960*N)) -nblocks $((N)) -nblocksz $((N)) -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
	echo "############## CM ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=cuthillmckee REVERSE=1 STARPU_NTASKS_THRESHOLD=30 STARPU_CUDA_PIPELINE=30 STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xyz $((960*N)) -nblocks $((N)) -nblocksz $((N)) -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
	#~ echo "############## HFP U TH30 MULTIGPU0 + TASK STEALING ##############"
	#~ for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		#~ do 
		#~ N=$((START_X+i*ECHELLE_X))
		#~ STARPU_SCHED=HFP STARPU_SCHED_READY=1 STARPU_NTASKS_THRESHOLD=30 TASK_STEALING=3 STARPU_SCHED_READY=1 STARPU_CUDA_PIPELINE=30 MULTIGPU=1 ORDER_U=1 STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xyz $((960*N)) -nblocks $((N)) -nblocksz $((N)) -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	#~ done
	echo "############## HFP U TH30 MULTIGPU4 + TASK STEALING ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=HFP STARPU_SCHED_READY=1 STARPU_NTASKS_THRESHOLD=30 TASK_STEALING=3 STARPU_SCHED_READY=1 STARPU_CUDA_PIPELINE=30 MULTIGPU=4 ORDER_U=1 STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xyz $((960*N)) -nblocks $((N)) -nblocksz $((N)) -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
	echo "############## Modular heft idle HFP U MULTIGPU4 TH0 ##############"
	for ((i=1 ; i<=(($NB_TAILLE_TESTE)); i++))
		do 
		N=$((START_X+i*ECHELLE_X))
		STARPU_SCHED=modular-heft-HFP STARPU_SCHED_READY=1 MODULAR_HEFT_HFP_MODE=2 MULTIGPU=4 STARPU_NTASKS_THRESHOLD=0 STARPU_CUDA_PIPELINE=30 ORDER_U=1 STARPU_SIMGRID_CUDA_MALLOC_COST=0 STARPU_LIMIT_BANDWIDTH=${BW} STARPU_MINIMUM_CLEAN_BUFFERS=0 STARPU_TARGET_CLEAN_BUFFERS=0 STARPU_LIMIT_CUDA_MEM=${CUDAMEM} STARPU_NCPU=0 STARPU_NCUDA=${NGPU} STARPU_NOPENCL=0 STARPU_HOSTNAME=attila ./examples/mult/sgemm -3d -xyz $((960*N)) -nblocks $((N)) -nblocksz $((N)) -iter 1 | tail -n 1 >> ${FICHIER_RAW:0}
	done
fi

# Tracage des GFlops
gcc -o cut_gflops_raw_out cut_gflops_raw_out.c
./cut_gflops_raw_out $NB_TAILLE_TESTE $NB_ALGO_TESTE $ECHELLE_X $START_X ${FICHIER_RAW:0} ${PATH_R}/R/Data/${DOSSIER}/${FICHIER}.txt
Rscript ${PATH_R}/R/ScriptR/${DOSSIER}/GF_NT_M3D_3GPU_BW1050_CM250_VERSION_HFP.R ${PATH_R}/R/Data/${DOSSIER}/${FICHIER}.txt ${Z} ${NGPU} ${CUDAMEM}
mv ${PATH_STARPU}/starpu/Rplots.pdf ${PATH_R}/R/Courbes/${DOSSIER}/${FICHIER}.pdf
#~ Rscript ${PATH_R}/R/ScriptR/${DOSSIER}/GF_NT_M3D_3GPU_BW1050_CM250_VERSION_HEFT.R ${PATH_R}/R/Data/${DOSSIER}/${FICHIER}_${Z}_${NGPU}.txt ${Z} ${NGPU}
#~ mv ${PATH_STARPU}/starpu/Rplots.pdf ${PATH_R}/R/Courbes/${DOSSIER}/GF_NT_M3D_3GPU_BW1050_CM250_VERSION_HEFT_${Z}_${NGPU}.pdf
